#ifndef JWLIST_DOUBLE_H_H_H
#define JWLIST_DOUBLE_H_H_H

#include "JWList_base.h"

/************************************************************************/
/* 线性表结构定义                                                               */
/************************************************************************/
typedef double					JWListElem_double;

typedef struct tagJWListNode_double
{
	JWListElem_double				elem;		//数据元素
	struct tagJWListNode_double	*pNext;		//下一个节点指针
	struct tagJWListNode_double	*pPrior;	//上一个节点指针
}JWListNode_double, *PJWListNode_double;

typedef struct
{
	int						nLength;	//数据元素
	JWListNode_double			*pNext;		//下一个节点指针
}JWList_double, *PJWList_double;

typedef JWList_BOOL (*JWList_CompareFunc_double)(JWListElem_double elem1, JWListElem_double elem2);
typedef JWList_BOOL (*JWList_TraverseFunc_double)(JWListElem_double *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基本线性表函数声明                                                           */
	/************************************************************************/
	JWList_double* JWListCreateFromHead_double(int nSize, JWListElem_double *pElemList);
	JWList_double* JWListCreateFromTail_double(int nSize, JWListElem_double *pElemList);
	void JWListDestroy_double(JWList_double *pList);
	void JWListMakeEmpty_double(JWList_double *pList);

	JWList_BOOL JWListIsEmpty_double(JWList_double *pList);
	int JWListGetLength_double(JWList_double *pList);

	JWList_BOOL JWListGetAt_double(JWList_double *pList, const int nIndex, JWListElem_double *pElem);
	JWList_BOOL JWListSetAt_double(JWList_double *pList, const int nIndex, const JWListElem_double elem);
	JWList_BOOL JWListInsert_double(JWList_double *pList, const int nIndex, const JWListElem_double elem);
	JWList_BOOL JWListDelete_double(JWList_double *pList, const int nIndex, JWListElem_double *pElem);
	PJWListNode_double JWListLocate_double( JWList_double *pList, const JWListElem_double elem, JWList_CompareFunc_double pCompare );
	void JWListTraverse_double(JWList_double *pList, JWList_TraverseFunc_double pTraverse);
	PJWListNode_double JWListGetPrior_double(JWListNode_double *pNode);

	JWList_BOOL JWListPrintfElem_double(JWListElem_double *pElem);
	void JWListDump_double(JWList_double *pList);

	/************************************************************************/
	/* 栈操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetTop_double( JWList_double *pList, const JWListElem_double elem );
	JWList_BOOL JWListGetTop_double( JWList_double *pList, JWListElem_double *pElem );
	JWList_BOOL JWListPush_double( JWList_double *pList, const JWListElem_double elem );
	JWList_BOOL JWListPop_double( JWList_double *pList, JWListElem_double *pElem );

	/************************************************************************/
	/* 队列操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetHead_double( JWList_double *pList, const JWListElem_double elem );
	JWList_BOOL JWListGetHead_double( JWList_double *pList, JWListElem_double *pElem );
	JWList_BOOL JWListEnQueue_double( JWList_double *pList, const JWListElem_double elem );
	JWList_BOOL JWListDeQueue_double( JWList_double *pList, JWListElem_double *pElem );

#ifdef __cplusplus
};
#endif

#endif