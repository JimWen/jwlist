#include <stdio.h>
#include <stdlib.h>

#include "JWList_double.h"

/*######################################################################*/
/*######################################################################*/
/* 基础线性表函数声明                                                           */
/*######################################################################*/
/*######################################################################*/

/************************************************************************/
/* 从头部开始创建链表                                                           */
/************************************************************************/
JWList_double* JWListCreateFromHead_double(int nSize, JWListElem_double *pElemList)
{
	PJWList_double pHead;
	PJWListNode_double pRear, pNode;
	int i;

	//创建一个头结点
	if (NULL == (pHead = (PJWList_double)malloc(sizeof(JWList_double))))
	{
		return NULL;
	}
	pHead->nLength	= 0;
	pHead->pNext	= NULL;

	if (0 == nSize)
	{
		return pHead;
	}

	//nSize不为0，在尾结点后添加后续节点
	for (i = 0; i < nSize; i++)
	{
		if (NULL == (pNode = (PJWListNode_double)malloc(sizeof(JWListNode_double))))
		{
			return pHead;
		}

		pNode->elem		= pElemList[i];

		if (0 == i)
		{
			pHead->pNext	= pNode;

			pNode->pPrior	= NULL;
			pNode->pNext	= NULL;

			pRear			= pNode;
		}
		else
		{
			pNode->pPrior	= pRear;
			pNode->pNext	= pRear->pNext;
			pRear->pNext	= pNode;

			pRear			= pNode;
		}

		pHead->nLength += 1;
	}

	return pHead;
}

/************************************************************************/
/* 从尾部开始创建链表                                                           */
/************************************************************************/
JWList_double* JWListCreateFromTail_double(int nSize, JWListElem_double *pElemList)
{
	PJWList_double pHead;
	PJWListNode_double pNode;
	int i;

	//创建一个头结点
	if (NULL == (pHead = (PJWList_double)malloc(sizeof(JWList_double))))
	{
		return NULL;
	}
	pHead->nLength	= 0;
	pHead->pNext	= NULL;

	if (0 == nSize)
	{
		return pHead;
	}

	//nSize不为0，在头结点后添加后续节点
	for (i = 0; i < nSize; i++)
	{
		if (NULL == (pNode = (PJWListNode_double)malloc(sizeof(JWListNode_double))))
		{
			return pHead;
		}
		pNode->elem = pElemList[i];

		if (i > 0)
		{
			pHead->pNext->pPrior	= pNode;
		}
		pNode->pNext			= pHead->pNext;
		pNode->pPrior			= NULL;
		pHead->pNext			= pNode;

		pHead->nLength += 1;
	}

	return pHead;
}

/************************************************************************/
/*销毁整个链表                                                                  */
/************************************************************************/
void JWListDestroy_double( JWList_double *pList )
{
	PJWListNode_double pNodePrev, pNode;

	pNode = pList->pNext;

	//删除所有元素节点
	while (pNode != NULL)
	{
		pNodePrev = pNode;
		pNode = pNode->pNext;

		free(pNodePrev);
	}

	//删除头节点
	free(pList);
}

/************************************************************************/
/* 清空整个链表                                                                 */
/************************************************************************/
void JWListMakeEmpty_double( JWList_double *pList )
{
	PJWListNode_double pNodePrev, pNode;

	pNode = pList->pNext;

	//删除所有元素节点
	while (pNode != NULL)
	{
		pNodePrev = pNode;
		pNode = pNode->pNext;

		free(pNodePrev);
	}

	pList->nLength	= 0;
	pList->pNext	= NULL;
}

JWList_BOOL JWListIsEmpty_double( JWList_double *pList )
{
	return 0 == pList->nLength ? JWLIST_TRUE : JWLIST_FALSE;
}

int JWListGetLength_double( JWList_double *pList )
{
	return pList->nLength;
}

/************************************************************************/
/* 获得指定索引的节点(0~长度-1)的值                                            */
/************************************************************************/
JWList_BOOL JWListGetAt_double( JWList_double *pList, const int nIndex, JWListElem_double *pElem )
{
	int i;
	JWListNode_double *pNode;

	//检查索引合法性，包含表为空的情形
	if (nIndex < 0 || nIndex > pList->nLength-1)
	{
		return JWLIST_FALSE;
	}

	//从头结点遍历到指定索引的节点
	pNode = pList->pNext;
	for (i = 1; i <= nIndex; i++)
	{
		pNode = pNode->pNext;
	}

	//获取指定节点的值
	*pElem = pNode->elem;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 设置指定索引的节点(0~长度-1)的值                                            */
/************************************************************************/
JWList_BOOL JWListSetAt_double( JWList_double *pList, const int nIndex, const JWListElem_double elem )
{
	int i;
	JWListNode_double *pNode;

	//检查索引合法性，包含表为空的情形
	if (nIndex < 0 || nIndex > pList->nLength-1)
	{
		return JWLIST_FALSE;
	}

	//从头结点遍历到指定索引的节点
	pNode = pList->pNext;
	for (i = 1; i <= nIndex; i++)
	{
		pNode = pNode->pNext;
	}

	//设置指定节点的值
	pNode->elem = elem;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 在指定索引的节点(0~长度)前插入新节点，索引==长度时表示在末尾添加新节点      */
/************************************************************************/
JWList_BOOL JWListInsert_double( JWList_double *pList, const int nIndex, const JWListElem_double elem )
{
	int i;
	JWListNode_double *pNodePrev, *pNode, *pNewNode;

	//检查索引合法性，包含表为空的情形
	if (nIndex < 0 || nIndex > pList->nLength)
	{
		return JWLIST_FALSE;
	}

	//从头结点遍历到指定索引的节点并记录其前驱节点
	pNode		= pList->pNext;
	pNodePrev	= NULL;
	for (i = 1; i <= nIndex; i++)
	{
		pNodePrev = pNode;
		pNode = pNode->pNext;
	}

	//添加指定节点
	if(NULL == (pNewNode = (PJWListNode_double)malloc(sizeof(JWListNode_double))))
	{
		return JWLIST_FALSE;
	}
	pNewNode->elem		= elem;
	pNewNode->pNext		= pNode;
	pNewNode->pPrior	= pNodePrev;

	if (pNodePrev != NULL)//不是在第一个节点前插入
	{
		pNodePrev->pNext = pNewNode;
	}
	else
	{
		pList->pNext = pNewNode;
	}
	if (NULL != pNode)//不是在末尾插入
	{
		pNode->pPrior	 = pNewNode;
	}

	pList->nLength += 1;//长度+1

	return JWLIST_TRUE;
}

/************************************************************************/
/* 删除指定索引的节点(0~长度-1)                                           */
/************************************************************************/
JWList_BOOL JWListDelete_double( JWList_double *pList, const int nIndex, JWListElem_double *pElem )
{
	int i;
	JWListNode_double *pNodePrev, *pNode;

	//检查索引合法性，包含表为空的情形
	if (nIndex < 0 || nIndex > pList->nLength-1)
	{
		return JWLIST_FALSE;
	}

	//从头结点遍历到指定索引的节点并记录其前驱节点
	pNode = pList->pNext;
	pNodePrev = NULL;
	for (i = 1; i <= nIndex; i++)
	{
		pNodePrev = pNode;
		pNode = pNode->pNext;
	}

	if (pNodePrev != NULL)//不是删除第一个节点
	{
		pNodePrev->pNext = pNode->pNext;
	}
	else
	{
		pList->pNext = pNode->pNext;
	}

	if(NULL != pNode->pNext)//不是删除末尾节点
	{
		pNode->pNext->pPrior = pNodePrev;
	}

	pList->nLength	-= 1;//长度-1

	if (NULL != pElem)
	{
		*pElem = pNode->elem;
	}
	free(pNode);

	return JWLIST_TRUE;
}

/************************************************************************/
/* 在线性表中遍历找到满足pCompare函数的索引返回索引指针，没找到则返回NULL     */
/************************************************************************/
PJWListNode_double JWListLocate_double( JWList_double *pList, const JWListElem_double elem, JWList_CompareFunc_double pCompare )
{
	PJWListNode_double pNode;

	//跳过头结点
	pNode = pList->pNext;

	//遍历对比余下所有节点
	while (pNode != NULL)
	{
		if (JWLIST_TRUE == pCompare(pNode->elem, elem))//找到满足要求的索引项
		{
			return pNode;
		}

		pNode = pNode->pNext;
	}

	return NULL;
}

/************************************************************************/
/* 在线性表中遍历对每个遍历到的元素调用pTraverse函数				         */
/************************************************************************/
void JWListTraverse_double(JWList_double *pList, JWList_TraverseFunc_double pTraverse)
{
	PJWListNode_double pNode;

	//跳过头结点
	pNode = pList->pNext;

	//遍历对比余下所有节点
	while (pNode != NULL)
	{
		pTraverse(&(pNode->elem));

		pNode = pNode->pNext;
	}
}

/************************************************************************/
/* 获得线性表中指定节点的前一个节点										    */
/************************************************************************/
PJWListNode_double JWListGetPrior_double(JWListNode_double *pNode)
{
	return pNode->pPrior;
}

/************************************************************************/
/* 定义单个元素的输出，对不同的JWArrayElem输出要更改该参数,主要用于调试       */
/************************************************************************/
JWList_BOOL JWListPrintfElem_double(JWListElem_double *pElem)
{
	printf("%f\t", *pElem);

	return JWLIST_TRUE;
}

/************************************************************************/
/* 输出当前线性表的详情，主要用于调试                                           */
/************************************************************************/
void JWListDump_double( JWList_double *pList)
{
	int i;
	PJWListNode_double pNode;

	printf("当前链式线性表数据元素类型为double型\n");

	//判断是否为空
	if (pList->nLength == 0)
	{
		printf("当前链式线性表为空\n\n");
		return;
	}

	//打印当前顺序线性表数据
	printf("当前链式线性表值列表详情如下:\n");
	printf("是否为空:%s\n", JWLIST_TRUE == JWListIsEmpty_double(pList) ? "空" : "非空");
	printf("长度:%d\n", JWListGetLength_double(pList));
	printf("值列表:\n");

	pNode = pList->pNext;
	i = 0;
	while (NULL != pNode)
	{
		JWListPrintfElem_double(&(pNode->elem));

		//每五个数据一行
		if ((i+1)%5 == 0)
		{
			printf("\n");
		}

		//指针指向下一个节点
		pNode = pNode->pNext;
		i++;
	}
	printf("\n\n");
}

/*######################################################################*/
/*######################################################################*/
/* 栈操作函数声明															*/
/*######################################################################*/
/*######################################################################*/

/************************************************************************/
/* 获得当前栈顶元素的值
** 注意在这里最靠近头结点的节点为栈顶元素									*/
/************************************************************************/
JWList_BOOL JWListGetTop_double( JWList_double *pList, JWListElem_double *pElem )
{
	//判断栈为空的情形
	if (NULL == pList->pNext)
	{
		return JWLIST_FALSE;
	}

	//获取栈顶元素值
	*pElem = pList->pNext->elem;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 设置当前栈顶元素的值
** 注意在这里最靠近头结点的节点为栈顶元素									*/
/************************************************************************/
JWList_BOOL JWListSetTop_double( JWList_double *pList, const JWListElem_double elem )
{
	//判断栈为空的情形
	if (NULL == pList->pNext)
	{
		return JWLIST_FALSE;
	}

	//设置栈顶元素值
	pList->pNext->elem = elem;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 当前栈入栈一个元素										                  */
/************************************************************************/
JWList_BOOL JWListPush_double( JWList_double *pList, const JWListElem_double elem )
{
	JWListNode_double *pFirstNode, *pNode;

	pFirstNode = pList->pNext;

	//添加指定节点
	if(NULL == (pNode = (PJWListNode_double)malloc(sizeof(JWListNode_double))))
	{
		return JWLIST_FALSE;
	}
	pNode->elem			= elem;

	//修改相关指针
	pList->pNext		= pNode;
	pNode->pNext		= pFirstNode;

	pNode->pPrior			= NULL;
	if (NULL != pFirstNode)//不是在末尾插入
	{
		pFirstNode->pPrior	= pNode;
	}

	//长度+1
	pList->nLength += 1;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 当前栈出栈一个元素			                                                 */
/************************************************************************/
JWList_BOOL JWListPop_double( JWList_double *pList, JWListElem_double *pElem )
{
	JWListNode_double *pFirstNode;

	pFirstNode = pList->pNext;

	//判断栈为空的情形
	if (NULL == pFirstNode)
	{
		return JWLIST_FALSE;
	}

	//删除栈顶元素(修改相关指针)
	pList->pNext = pFirstNode->pNext;
	if(NULL != pFirstNode->pNext)//不是删除末尾节点
	{
		pFirstNode->pNext->pPrior = NULL;
	}

	//长度-1
	pList->nLength -= 1;

	//获取栈顶元素
	if (NULL != pElem)
	{
		*pElem = pFirstNode->elem;
	}

	//清除栈顶元素内存
	free(pFirstNode);

	return JWLIST_TRUE;
}

/*######################################################################*/
/*######################################################################*/
/* 队列操作函数声明													    */
/*######################################################################*/
/*######################################################################*/

/************************************************************************/
/* 获得当前队列头元素的值
** 注意在这里最靠近头结点的节点为队列头元素									*/
/************************************************************************/
JWList_BOOL JWListGetHead_double( JWList_double *pList, JWListElem_double *pElem )
{
	//判断栈为空的情形
	if (NULL == pList->pNext)
	{
		return JWLIST_FALSE;
	}

	//获取栈顶元素值
	*pElem = pList->pNext->elem;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 设置当前队列元素的值
** 注意在这里最靠近头结点的节点为队列头元素									*/
/************************************************************************/
JWList_BOOL JWListSetHead_double( JWList_double *pList, const JWListElem_double elem )
{
	//判断栈为空的情形
	if (NULL == pList->pNext)
	{
		return JWLIST_FALSE;
	}

	//设置栈顶元素值
	pList->pNext->elem = elem;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 当前队列入列一个元素										              */
/************************************************************************/
JWList_BOOL JWListEnQueue_double( JWList_double *pList, const JWListElem_double elem )
{
	JWListNode_double *pNewNode, *pNode;

	pNode = pList->pNext;

	//添加指定节点
	if(NULL == (pNewNode = (PJWListNode_double)malloc(sizeof(JWListNode_double))))
	{
		return JWLIST_FALSE;
	}
	pNewNode->elem			= elem;

	if (NULL == pNode)//当前队列为空
	{
		pNewNode->pNext = NULL;
		pNewNode->pPrior = NULL;
		pList->pNext = pNewNode;
	}
	else
	{
		//找到最后一个元素节点
		while (NULL != pNode->pNext)
		{
			pNode = pNode->pNext;
		}

		pNewNode->pNext = NULL;
		pNewNode->pPrior = pNode;
		pNode->pNext = pNewNode;
	}

	//长度+1
	pList->nLength += 1;

	return JWLIST_TRUE;
}

/************************************************************************/
/* 当前队列出列一个元素			                                                 */
/************************************************************************/
JWList_BOOL JWListDeQueue_double( JWList_double *pList, JWListElem_double *pElem )
{
	JWListNode_double *pFirstNode;

	pFirstNode = pList->pNext;

	//判断队列为空的情形
	if (NULL == pFirstNode)
	{
		return JWLIST_FALSE;
	}

	//删除队列头元素(修改相关指针)
	pList->pNext = pFirstNode->pNext;
	if(NULL != pFirstNode->pNext)//不是删除末尾节点
	{
		pFirstNode->pNext->pPrior = NULL;
	}

	//长度-1
	pList->nLength -= 1;

	//获取队列头元素
	if (NULL != pElem)
	{
		*pElem = pFirstNode->elem;
	}

	//清除队列头元素内存
	free(pFirstNode);

	return JWLIST_TRUE;
}