#ifndef JWLIST_CHAR_H_H_H
#define JWLIST_CHAR_H_H_H

#include "JWList_base.h"

/************************************************************************/
/* 线性表结构定义                                                               */
/************************************************************************/
typedef char					JWListElem_char;

typedef struct tagJWListNode_char
{
	JWListElem_char				elem;		//数据元素
	struct tagJWListNode_char	*pNext;		//下一个节点指针
	struct tagJWListNode_char	*pPrior;	//上一个节点指针
}JWListNode_char, *PJWListNode_char;

typedef struct
{
	int						nLength;	//数据元素
	JWListNode_char			*pNext;		//下一个节点指针
}JWList_char, *PJWList_char;

typedef JWList_BOOL (*JWList_CompareFunc_char)(JWListElem_char elem1, JWListElem_char elem2);
typedef JWList_BOOL (*JWList_TraverseFunc_char)(JWListElem_char *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基本线性表函数声明                                                           */
	/************************************************************************/
	JWList_char* JWListCreateFromHead_char(int nSize, JWListElem_char *pElemList);
	JWList_char* JWListCreateFromTail_char(int nSize, JWListElem_char *pElemList);
	void JWListDestroy_char(JWList_char *pList);
	void JWListMakeEmpty_char(JWList_char *pList);

	JWList_BOOL JWListIsEmpty_char(JWList_char *pList);
	int JWListGetLength_char(JWList_char *pList);

	JWList_BOOL JWListGetAt_char(JWList_char *pList, const int nIndex, JWListElem_char *pElem);
	JWList_BOOL JWListSetAt_char(JWList_char *pList, const int nIndex, const JWListElem_char elem);
	JWList_BOOL JWListInsert_char(JWList_char *pList, const int nIndex, const JWListElem_char elem);
	JWList_BOOL JWListDelete_char(JWList_char *pList, const int nIndex, JWListElem_char *pElem);
	PJWListNode_char JWListLocate_char( JWList_char *pList, const JWListElem_char elem, JWList_CompareFunc_char pCompare );
	void JWListTraverse_char(JWList_char *pList, JWList_TraverseFunc_char pTraverse);
	PJWListNode_char JWListGetPrior_char(JWListNode_char *pNode);

	JWList_BOOL JWListPrintfElem_char(JWListElem_char *pElem);
	void JWListDump_char(JWList_char *pList);

	/************************************************************************/
	/* 栈操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetTop_char( JWList_char *pList, const JWListElem_char elem );
	JWList_BOOL JWListGetTop_char( JWList_char *pList, JWListElem_char *pElem );
	JWList_BOOL JWListPush_char( JWList_char *pList, const JWListElem_char elem );
	JWList_BOOL JWListPop_char( JWList_char *pList, JWListElem_char *pElem );

	/************************************************************************/
	/* 队列操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetHead_char( JWList_char *pList, const JWListElem_char elem );
	JWList_BOOL JWListGetHead_char( JWList_char *pList, JWListElem_char *pElem );
	JWList_BOOL JWListEnQueue_char( JWList_char *pList, const JWListElem_char elem );
	JWList_BOOL JWListDeQueue_char( JWList_char *pList, JWListElem_char *pElem );

#ifdef __cplusplus
};
#endif

#endif