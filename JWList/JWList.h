/********************************************************************
	创建日期:	2014/04/14
	包含文件:	JWList.h
	  创建者:	Jim Wen
        版本:	V1.4
	
		功能:	实现了双向链式存储结构的线性表、栈、队列
		说明:	1.V1.4相对于V1.3加入泛型
				2.线性表的从紧邻头结点开始
				3.栈的栈顶为紧邻头结点
				4.队列头节点为为紧邻头结点
				5.这里的泛型支持就是对每种类型实现一次
				6.自定义类型时，首先更改JWListElem定义，然后复制一个已有的类型如int，替换_int即可
				要注意JWListPrintfElem、JWListDump、JWList_CompareFunc、JWList_TraverseFunc
				的细微变化
				7.JWListElem的数据元素类型是字符串或包括字符串的结构体时，还
				要对赋值和移动操作有些改变--如=要换成strlen函数，一般不使用
				字符串作为数据元素，一旦使用要注意!!!。
*********************************************************************/

#ifndef JWLIST_H_H_H
#define JWLIST_H_H_H

#include "JWList_base.h"
#include "JWList_int.h"
#include "JWList_char.h"
#include "JWList_double.h"
#include "JWList_float.h"

#define JWListElem(type)		JWListElem_##type
#define JWListNode(type)		JWListNode_##type
#define PJWListNode(type)		PJWListNode_##type
#define JWList(type)			JWList_##type
#define PJWList(type)			PJWList_##type

/************************************************************************/
/* 基本线性表函数声明                                                           */
/************************************************************************/
#define JWListCreateFromHead(type)		JWListCreateFromHead_##type
#define JWListCreateFromTail(type)		JWListCreateFromTail_##type
#define JWListDestroy(type)				JWListDestroy_##type
#define JWListMakeEmpty(type)			JWListMakeEmpty_##type

#define JWListIsEmpty(type)				JWListIsEmpty_##type
#define JWListGetLength(type)			JWListGetLength_##type

#define JWListGetAt(type)				JWListGetAt_##type
#define JWListSetAt(type)				JWListSetAt_##type
#define JWListInsert(type)				JWListInsert_##type
#define JWListDelete(type)				JWListDelete_##type
#define JWListLocate(type)				JWListLocate_##type
#define JWListTraverse(type)			JWListTraverse_##type
#define JWListGetPrior(type)			JWListGetPrior_##type

#define JWListPrintfElem(type)			JWListPrintfElem_##type
#define JWListDump(type)				JWListDump_##type

#define JWList_CompareFunc(type)		JWList_CompareFunc_##type
#define JWList_TraverseFunc(type)		JWList_TraverseFunc_##type

/************************************************************************/
/* 栈操作函数声明			                                                     */
/************************************************************************/
#define JWListSetTop(type)				JWListSetTop_##type
#define JWListGetTop(type)				JWListGetTop_##type
#define JWListPush(type)				JWListPush_##type
#define JWListPop(type)					JWListPop_##type

/************************************************************************/
/* 队列操作函数声明			                                                 */
/************************************************************************/
#define JWListSetHead(type)				JWListSetHead_##type
#define JWListGetHead(type)				JWListGetHead_##type
#define JWListEnQueue(type)				JWListEnQueue_##type
#define JWListDeQueue(type)				JWListDeQueue_##type

#endif