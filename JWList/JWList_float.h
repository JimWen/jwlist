#ifndef JWLIST_FLOAT_H_H_H
#define JWLIST_FLOAT_H_H_H

#include "JWList_base.h"

/************************************************************************/
/* 线性表结构定义                                                               */
/************************************************************************/
typedef float				JWListElem_float;

typedef struct tagJWListNode_float
{
	JWListElem_float			elem;		//数据元素
	struct tagJWListNode_float	*pNext;		//下一个节点指针
	struct tagJWListNode_float	*pPrior;	//上一个节点指针
}JWListNode_float, *PJWListNode_float;

typedef struct
{
	int						nLength;	//数据元素
	JWListNode_float			*pNext;		//下一个节点指针
}JWList_float, *PJWList_float;

typedef JWList_BOOL (*JWList_CompareFunc_float)(JWListElem_float elem1, JWListElem_float elem2);
typedef JWList_BOOL (*JWList_TraverseFunc_float)(JWListElem_float *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基本线性表函数声明                                                           */
	/************************************************************************/
	JWList_float* JWListCreateFromHead_float(int nSize, JWListElem_float *pElemList);
	JWList_float* JWListCreateFromTail_float(int nSize, JWListElem_float *pElemList);
	void JWListDestroy_float(JWList_float *pList);
	void JWListMakeEmpty_float(JWList_float *pList);

	JWList_BOOL JWListIsEmpty_float(JWList_float *pList);
	int JWListGetLength_float(JWList_float *pList);

	JWList_BOOL JWListGetAt_float(JWList_float *pList, const int nIndex, JWListElem_float *pElem);
	JWList_BOOL JWListSetAt_float(JWList_float *pList, const int nIndex, const JWListElem_float elem);
	JWList_BOOL JWListInsert_float(JWList_float *pList, const int nIndex, const JWListElem_float elem);
	JWList_BOOL JWListDelete_float(JWList_float *pList, const int nIndex, JWListElem_float *pElem);
	PJWListNode_float JWListLocate_float( JWList_float *pList, const JWListElem_float elem, JWList_CompareFunc_float pCompare );
	void JWListTraverse_float(JWList_float *pList, JWList_TraverseFunc_float pTraverse);
	PJWListNode_float JWListGetPrior_float(JWListNode_float *pNode);

	JWList_BOOL JWListPrintfElem_float(JWListElem_float *pElem);
	void JWListDump_float(JWList_float *pList);

	/************************************************************************/
	/* 栈操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetTop_float( JWList_float *pList, const JWListElem_float elem );
	JWList_BOOL JWListGetTop_float( JWList_float *pList, JWListElem_float *pElem );
	JWList_BOOL JWListPush_float( JWList_float *pList, const JWListElem_float elem );
	JWList_BOOL JWListPop_float( JWList_float *pList, JWListElem_float *pElem );

	/************************************************************************/
	/* 队列操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetHead_float( JWList_float *pList, const JWListElem_float elem );
	JWList_BOOL JWListGetHead_float( JWList_float *pList, JWListElem_float *pElem );
	JWList_BOOL JWListEnQueue_float( JWList_float *pList, const JWListElem_float elem );
	JWList_BOOL JWListDeQueue_float( JWList_float *pList, JWListElem_float *pElem );

#ifdef __cplusplus
};
#endif

#endif