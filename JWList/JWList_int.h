#ifndef JWLIST_INT_H_H_H
#define JWLIST_INT_H_H_H

#include "JWList_base.h"

/************************************************************************/
/* 线性表结构定义                                                               */
/************************************************************************/
typedef int					JWListElem_int;

typedef struct tagJWListNode_int
{
	JWListElem_int				elem;		//数据元素
	struct tagJWListNode_int	*pNext;		//下一个节点指针
	struct tagJWListNode_int	*pPrior;	//上一个节点指针
}JWListNode_int, *PJWListNode_int;

typedef struct
{
	int						nLength;	//数据元素
	JWListNode_int			*pNext;		//下一个节点指针
}JWList_int, *PJWList_int;

typedef JWList_BOOL (*JWList_CompareFunc_int)(JWListElem_int elem1, JWListElem_int elem2);
typedef JWList_BOOL (*JWList_TraverseFunc_int)(JWListElem_int *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基本线性表函数声明                                                           */
	/************************************************************************/
	JWList_int* JWListCreateFromHead_int(int nSize, JWListElem_int *pElemList);
	JWList_int* JWListCreateFromTail_int(int nSize, JWListElem_int *pElemList);
	void JWListDestroy_int(JWList_int *pList);
	void JWListMakeEmpty_int(JWList_int *pList);

	JWList_BOOL JWListIsEmpty_int(JWList_int *pList);
	int JWListGetLength_int(JWList_int *pList);

	JWList_BOOL JWListGetAt_int(JWList_int *pList, const int nIndex, JWListElem_int *pElem);
	JWList_BOOL JWListSetAt_int(JWList_int *pList, const int nIndex, const JWListElem_int elem);
	JWList_BOOL JWListInsert_int(JWList_int *pList, const int nIndex, const JWListElem_int elem);
	JWList_BOOL JWListDelete_int(JWList_int *pList, const int nIndex, JWListElem_int *pElem);
	PJWListNode_int JWListLocate_int( JWList_int *pList, const JWListElem_int elem, JWList_CompareFunc_int pCompare );
	void JWListTraverse_int(JWList_int *pList, JWList_TraverseFunc_int pTraverse);
	PJWListNode_int JWListGetPrior_int(JWListNode_int *pNode);

	JWList_BOOL JWListPrintfElem_int(JWListElem_int *pElem);
	void JWListDump_int(JWList_int *pList);

	/************************************************************************/
	/* 栈操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetTop_int( JWList_int *pList, const JWListElem_int elem );
	JWList_BOOL JWListGetTop_int( JWList_int *pList, JWListElem_int *pElem );
	JWList_BOOL JWListPush_int( JWList_int *pList, const JWListElem_int elem );
	JWList_BOOL JWListPop_int( JWList_int *pList, JWListElem_int *pElem );

	/************************************************************************/
	/* 队列操作函数声明			                                                     */
	/************************************************************************/
	JWList_BOOL JWListSetHead_int( JWList_int *pList, const JWListElem_int elem );
	JWList_BOOL JWListGetHead_int( JWList_int *pList, JWListElem_int *pElem );
	JWList_BOOL JWListEnQueue_int( JWList_int *pList, const JWListElem_int elem );
	JWList_BOOL JWListDeQueue_int( JWList_int *pList, JWListElem_int *pElem );

#ifdef __cplusplus
};
#endif

#endif